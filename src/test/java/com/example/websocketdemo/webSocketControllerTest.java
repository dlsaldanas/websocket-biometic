package com.example.websocketdemo;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


public class webSocketControllerTest {
    @Mock
    BiometricHandler biometricHandler;
    @InjectMocks
    WebSocketController webSocketController;
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void ShouldPrepare() {
        Mockito.doNothing().when(biometricHandler).prepare();
        webSocketController.prepare();
        Mockito.verify(biometricHandler, Mockito.times(1)).prepare();
    }


    @Test
    public void shouldCapture() {
        Mockito.doNothing().when(biometricHandler).capture();
        webSocketController.capture();
        Mockito.verify(biometricHandler, Mockito.times(1)).capture();

    }

    @Test
    public void shouldAuth() throws Exception {
        Mockito.doNothing().when(biometricHandler).auth(Mockito.any(Object.class));
        webSocketController.auth();
    }
    @Test
    public void shouldProcess() {
        //Mockito.doNothing().when(biometricHandler).process();
        webSocketController.process();
    }
}