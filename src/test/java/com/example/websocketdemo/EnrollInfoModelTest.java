package com.example.websocketdemo;

import org.junit.Test;

public class EnrollInfoModelTest {

    private EnrollInfoModel enrollInfoModel = new EnrollInfoModel();
    @Test
    public void shouldSetAndGetRut(){
        String rut = "123";
        enrollInfoModel.setRut(rut);
        String result = enrollInfoModel.getRut();
        assert result.equals(rut);
    }

    @Test
    public void shouldSetAndGetRole(){
        String rol = "ejecutivo";
        enrollInfoModel.setRole(rol);
        String result = enrollInfoModel.getRole();
        assert result.equals(rol);

    }
    @Test
    public void shouldSetAndGetEnrolledFinger(){
        Integer enrolledFinger = 2;
        enrollInfoModel.setEnrolledFinger(enrolledFinger);
        assert enrollInfoModel.getEnrolledFinger().equals(enrolledFinger);
    }

    @Test
    public void shouldSetAndGetMinutiae(){
        Object minutiae = new Object();
        enrollInfoModel.setMinutiae(minutiae);
        assert enrollInfoModel.getMinutiae().equals(minutiae);
    }

    @Test
    public void shouldSetAndGetDefaultFinger(){
        Integer defaultFinger = 2;
        enrollInfoModel.setDefaultFinger(defaultFinger);
        assert enrollInfoModel.getDefaultFinger().equals(defaultFinger);
    }
}