package com.example.websocketdemo;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.config.SimpleBrokerRegistration;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.StompWebSocketEndpointRegistration;

public class WebSocketConfigurationTest {

    @Mock
    StompEndpointRegistry stompEndpointRegistryMock;
    @InjectMocks
    WebSocketConfiguration webSocketConfiguration;
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldRegisterStompEndpoints() {
        StompWebSocketEndpointRegistration value = Mockito.mock(StompWebSocketEndpointRegistration.class);
        Mockito.when(stompEndpointRegistryMock.addEndpoint(Mockito.any(String.class))).thenReturn(value);
        webSocketConfiguration.registerStompEndpoints(stompEndpointRegistryMock);
    }

    @Test
    public void shouldConfigureMessageBroker() {
        MessageBrokerRegistry messageBrokerRegistry = Mockito.mock(MessageBrokerRegistry.class);
        Mockito.when(messageBrokerRegistry.setApplicationDestinationPrefixes(Mockito.any(String.class))).thenReturn(messageBrokerRegistry);
        SimpleBrokerRegistration simple =Mockito.mock(SimpleBrokerRegistration.class);
        Mockito.when(messageBrokerRegistry.enableSimpleBroker(Mockito.any(String.class),Mockito.any(String.class))).thenReturn(simple);
        webSocketConfiguration.configureMessageBroker(messageBrokerRegistry);
    }
}