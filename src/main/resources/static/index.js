var stompClient = null;

function setConnected(connected) {
    $("#connectBtn").prop("disabled", connected);
    $("#disconnectBtn").prop("disabled", !connected);
    if (connected) {
        document.getElementById("ContanerFormConectionBtn").style.visibility='hidden';
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
        document.getElementById("ContanerFormConectionBtn").style.visibility='visible';
        document.getElementById("connectionInfo").innerHTML="Not conected to Websocket, please refresh page";
    }
    $("#info").html("");
}

function connect() {
    var socket = new SockJS('/portfolio');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        document.getElementById("connectionInfo").innerHTML="Conected to Websocket";
        stompClient.subscribe('/topic/info', function (greeting) {
            showGreeting(greeting.body);
        });
    });
}

function clearMessages(){
    document.getElementById("info").innerHTML="";
}

function capture(){
    //send(destination, headers = {}, body = '')
    console.log("front llama a capture")
    stompClient.send("/app/capture", {}, 'capture');
}
function init(){
    //send(destination, headers = {}, body = '')
    stompClient.send("/app/prepare", {}, 'init');
}
function auth(){
    //send(destination, headers = {}, body = '')
    stompClient.send("/app/auth", {}, 'auth');
}


function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendName() {
    console.log("stringfy" + JSON.stringify({'name': $("#name").val()}))
    stompClient.send("/app/greeting", {}, JSON.stringify({'name': $("#name").val()}));
}

function showGreeting(message) {
        $("#info").append("<tr><td>" + message + "</td></tr>");
}

window.onload = function(){
    connect();
    document.getElementById("ContanerFormConectionBtn").style.visibility='hidden';
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connectBtn" ).click(function() { connect(); });
    $( "#disconnectBtn" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });
    $( "#captureBtn" ).click(function() { capture(); })
    $( "#init" ).click(function() { init(); })
    $( "#auth" ).click(function() { auth(); })
    $('#clearBtn').click(function(){clearMessages()})


});