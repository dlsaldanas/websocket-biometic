package com.example.websocketdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;

@EnableScheduling
@Controller
public class WebSocketController {


    @Autowired
 private BiometricHandler biometricHandler;


    @MessageMapping("/prepare")
    @SendTo("/topic/info")
    public String prepare() {
        biometricHandler.prepare();
      return "prepare done";
    }

    @MessageMapping("/capture")
    @SendTo("/topic/info")
    public String capture() {
        biometricHandler.prepare();
        biometricHandler.capture();
        biometricHandler.write();
        biometricHandler.process();
        biometricHandler.halt();
        return "capture done";
    }

    @MessageMapping("/auth")
    @SendTo("/topic/info")
    public String auth() {
        //TODO asign object to compare
        //Object T = new Object();
        try {
            //biometricHandler.auth(T);
            return "auth done";
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }

    }

    @MessageMapping("/process")
    @SendTo("/topic/info")
    public String process() {
        return "process done";
    }



}