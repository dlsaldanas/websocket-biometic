package com.example.websocketdemo;



import com.falabella.fif.security.auth.BiometricAuth;
import com.falabella.fif.security.auth.exception.BiometricDeviceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;


@Service
public class BiometricHandler {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    BiometricAuth biometricAuth;

    public BiometricHandler() throws BiometricDeviceException {
        String currentDirectory = System.getProperty("user.dir");
        biometricAuth = new BiometricAuth(currentDirectory+"/device.config");
    }

    public void prepare()  {
        try {
            biometricAuth.prepare();
        } catch (Exception e) {
            messagingTemplate.convertAndSend("topic/info", "error en preparacion: " + e.getMessage());
        }
    }



    public void capture()  {
        try {
            biometricAuth.capture();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public <T> void auth(T toMatch)  {
        Boolean result = null;
        try {
            result = biometricAuth.auth(toMatch);
            messagingTemplate.convertAndSend("topic/info", "validacion retorna " + result);
        } catch (Exception e) {
            messagingTemplate.convertAndSend("topic/info", "error en auth: " + e.getMessage());
        }
    }

    public void write()  {
        String result = null;
        try {
            result = biometricAuth.write();
            messagingTemplate.convertAndSend("/topic/info","Imagen guardada en "+ result);
        } catch (Exception e) {
            messagingTemplate.convertAndSend("topic/info", "error en write: " + e.getMessage());
        }

    }

    public void process() {
        String result = null;
        try {
            result = biometricAuth.process();
            messagingTemplate.convertAndSend("/topic/info","WSQ guardada en "+ result);
        } catch (Exception e) {
            messagingTemplate.convertAndSend("topic/info", "error en process: " + e.toString());
        }

    }


    public void halt()  {
        try {
            biometricAuth.halt();
        } catch (Exception e) {
            messagingTemplate.convertAndSend("topic/info", "error en halt: " + e.getMessage());

        }
    }





}