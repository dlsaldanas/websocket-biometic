package com.example.websocketdemo;

public class EnrollInfoModel {
    private String rut;
    private String role;
    private Integer enrolledFinger;
    private Object minutiae;
    private Integer defaultFinger;


    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Integer getEnrolledFinger() {
        return enrolledFinger;
    }

    public void setEnrolledFinger(Integer enrolledFinger) {
        this.enrolledFinger = enrolledFinger;
    }

    public Object getMinutiae() {
        return minutiae;
    }

    public void setMinutiae(Object minutiae) {
        this.minutiae = minutiae;
    }

    public Integer getDefaultFinger() {
        return defaultFinger;
    }

    public void setDefaultFinger(Integer defaultFinger) {
        this.defaultFinger = defaultFinger;
    }

}
